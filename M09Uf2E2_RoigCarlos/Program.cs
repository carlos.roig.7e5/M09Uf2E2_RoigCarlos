﻿using System;
using System.Collections.Generic;
using System.Threading;
namespace M09Uf2E2_RoigCarlos
{
    class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {

            var prova = new Program();
            prova.Inici();
        }

        void Inici()
        {
            Console.WriteLine(Environment.CurrentDirectory);
            do
            {
                MostratMenu();
            } while (!OpcionsMenu());
        }

        void MostratMenu()
        {
            InOutData.ShowString("[1]. Exercici1.");
            InOutData.ShowString("[2]. Exercici2.");
            InOutData.ShowString("[0]. Sortir.");
        }

        bool OpcionsMenu()
        {
            switch (InOutData.InString("Introdueix una opció del 0 al 1."))
            {
                case "0":
                    return true;
                case "1":
                    Exercici1();
                    break;
                case "2":
                    Exercici2();
                    break;
            }
            return false;
        }
        void Exercici1()
        {
            List<string[]> listString = new List<string[]>();
            listString.Add(new string[] { "Hola", "Buenas", "Tardes" });
            listString.Add(new string[] { "Hola", "Buenas", "Tardes" });
            listString.Add(new string[] { "Hola", "Buenas", "Tardes" });
            for (int i = 0; i < 3; i++)
            {
                ThreadWriteLines(listString[i]);
            }
        }

        void Exercici2()
        {
            var nevera = new Nevera(6);
            var threadOmplir = new Thread(nevera.OmplirNevera);
            threadOmplir.Start("Anitta");
            threadOmplir.Join();
            var threadBeure = new Thread(nevera.BeureCerveza);
            var noms = new string[] { "Bad Bunny", "Lil Nas X", "Manuel Turizo" };
            for (int i = 0; i < 3; i++)
            {
                ThreadNevera(noms[i], nevera);
            }
        }

        void ThreadWriteLines(string[] line)
        {
            var thread = new Thread(WriteLineByLine);
            thread.Start(line);
            thread.Join();
        }

        void ThreadNevera(string nom, Nevera nevera)
        {
            var thread = new Thread(nevera.BeureCerveza);
            thread.Start(nom);
            thread.Join();
        }

        void WriteLineByLine(object line)
        {
            var stringLine = (string[])line;
            foreach (var word in stringLine)
            {
                Console.Write(word+" ");
                Thread.Sleep(2000);
            }
            Console.WriteLine();
        }
    }
}
