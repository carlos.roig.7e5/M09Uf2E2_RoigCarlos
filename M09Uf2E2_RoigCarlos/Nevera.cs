﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M09Uf2E2_RoigCarlos
{
    class Nevera
    {
        private int _cervezas;
        public Nevera(int cervezas)
        {
            _cervezas = cervezas;
        }

        public void OmplirNevera(object nom)
        {
            var rnd = new Random();
            int cervezas = 0;
            for (int i = 0; i < rnd.Next(0,7)&&!SiEstaLlena(); i++)
            {
                _cervezas += 1;
                cervezas += 1;
            }
            Console.WriteLine("{0} ompli la nevera amb {1}",nom,cervezas);
        }

        public void BeureCerveza(object nom)
        {
            var rnd = new Random();
            int cervezas = 0;
            for (int i = 0; i < rnd.Next(0, 7) && !SiEstaVacia(); i++)
            {
                _cervezas -= 1;
                cervezas += 1;
            }
            Console.WriteLine("Després {0} begui {1}", nom, cervezas);
        }

        private bool SiEstaLlena()
        {
            if (_cervezas < 9)
                return false;
            return true;
        }

        private bool SiEstaVacia()
        {
            if (_cervezas > 0)
                return false;
            return true;
        }
    }
}
